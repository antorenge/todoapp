Feature: Session management
  In order to get access to my todo application
  As a user
  I want to be properly authenticated

  Scenario: Successful login
    Given a user "aslan" with password "xyz"
    And I am on the login page
    When I fill in "Username" with "aslan"
    And I fill in "Password" with "xyz"
    And I press "Log in"
    Then I should see "List of todo tasks"
  Scenario: Authentication failure
    Given a user "aslan" with password "xyz"
    And I am on the login page
    When I fill in "Username" with "aslan"
    And I fill in "Password" with "wrong"
    And I press "Log in"
    Then I should see "Authentication failed"
  Scenario: Successful login followed by logout
    Given a user "aslan" with password "xyz"
    And I am on the login page
    When I fill in "Username" with "aslan"
    And I fill in "Password" with "xyz"
    And I press "Log in"
    And I press "Log out"
    Then I should see "You have been logged out"
