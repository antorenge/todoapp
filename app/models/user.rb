class User < ActiveRecord::Base
  validates_presence_of :full_name, :username
  validates_uniqueness_of :username
  validates_confirmation_of :password
end
