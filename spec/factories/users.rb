FactoryGirl.define do
  factory :user do
    full_name { Faker::Name.name }
    sequence(:username) { |n| "username#{n}" }
    email { Faker::Internet.email }
    password "password"
    password_confirmation "password"
    confirmed_at Time.now

    role "user"
    factory :admin do
      role "admin"
    end
  end
end
